import { defineBuildConfig } from 'unbuild'

export default defineBuildConfig({
  name: 'piveau-universal-piwik',
  entries: [
    'src/piveau-universal-piwik.js',
  ],
  declaration: false,
  clean: true,
  rollup: {
    emitCJS: true,
  },
})
