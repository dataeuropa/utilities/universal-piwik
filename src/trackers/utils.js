function cookieDelete(cookieName) {
  document.cookie = `${cookieName}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
}

function deletePiwikCookies() {
  const allCookies = document.cookie.split(';');
  for (let cookie = 0; cookie < allCookies.length; cookie += 1) {
    if ((allCookies[cookie].indexOf('_pk') !== -1) || (allCookies[cookie].indexOf('ppms') !== -1)) {
      const cookieName = allCookies[cookie].split('=')[0];
      cookieDelete(cookieName);
    }
  }
}

function deletePiwikWebStorage() {
  window.localStorage.removeItem('ppms_webstorage');
}

export default function clearPiwik() {
  deletePiwikCookies();
  deletePiwikWebStorage();
}
