/* eslint-disable class-methods-use-this */
/* eslint-disable no-underscore-dangle */

export default class MatomoTracker {
  constructor(trackerUrl, siteId, options) {
    window._paq = window._paq || [];
    this.type = 'matomo';
    this.trackerUrl = trackerUrl.slice(-1) === '/' ? trackerUrl : `${trackerUrl}/`;
    this.siteId = siteId;
    this.stopped = false;
    this.options = {
      debug: false,
      disabled: false,
      requireConsent: 'consent',
      ...options,
    };

    if (this.options.disabled) {
      return new Proxy(this, {
        get(target, prop) {
          const origMethod = target[prop];
          if (typeof origMethod === 'function') {
            return () => {};
          }
          return undefined;
        },
      });
    }
  }

  // eslint-disable-next-line class-methods-use-this
  _init() {
    if (this.options.debug) console.log('init', this.type);
    const u = this.trackerUrl;
    if (!!this.options.requireConsent)window._paq.push([this.options.requireConsent === 'cookieConsent' ? 'requireCookieConsent' : 'requireConsent']);
    window._paq.push(['setTrackerUrl', `${u}matomo.php`]);
    window._paq.push(['setSiteId', `${this.siteId}`]);
    const d = document;
    const g = d.createElement('script');
    const s = d.getElementsByTagName('script')[0];
    g.type = 'text/javascript';
    g.async = true;
    g.defer = true;
    g.src = `${u}matomo.js`;
    s.parentNode.insertBefore(g, s);
  }

  init() {
    if (!this.options.immediate) {
      // eslint-disable-next-line no-underscore-dangle
      this._init();
    }
  }

  consentGiven() {
    window._paq.push(['rememberConsentGiven']);
  }

  consentDeclined() {
    window._paq.push(['forgetConsentGiven']);
  }

  // eslint-disable-next-line class-methods-use-this
  consentNoDecision() {}

  trackInteraction() {}

  trackPageView(url, title) {
    if (url) window._paq.push(['setCustomUrl', url]);
    if (title) window._paq.push(['setDocumentTitle', title]);
    window._paq.push(['trackPageView']);
  }

  trackDatasetDetailsPageView() {}

  trackDownload(url, dimensions) {
    window._paq.push([
      'trackLink',
      url,
      'download',
      ...dimensions ? [dimensions] : [],
    ]);
  }

  trackOutlink(url, dimensions) {
    window._paq.push([
      'trackLink',
      url,
      'link',
      ...dimensions ? [dimensions] : [],
    ]);
  }

  trackEvent(category, action, name, value) {
    window._paq.push([
      'trackEvent',
      category,
      ...action ? [action] : [],
      ...name ? [name] : [],
      ...value ? [value] : []]);
  }

  trackGotoResource() {
    window._paq.push(['trackEvent', 'GoToResource', 'Clicked']);
  }

  stop() {
    if (this.options.debug) console.log('Disabling window._paq');
    window._paq = {
      push() {},
    };
    this.stopped = true;
  }

  get isStopped() {
    return !!this.stopped;
  }
}
