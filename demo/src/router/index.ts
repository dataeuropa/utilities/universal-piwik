import * as Router from 'vue-router';
import Home from '../views/Home.vue';

// import { createApp } from 'vue';
// import App from '../App.vue'
// const app = createApp(App);
// app.use(Router);

const routes = [
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
  },
];

const router = Router.createRouter({
  history: Router.createWebHistory('/'),
  routes,
});

router.beforeEach((to, from, next) => {
  if (!to.query || !to.query.locale) {
    next({name: to.name, query: { locale: 'en' }});
  } else {
    next()
  }
});

export default router;
