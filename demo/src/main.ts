import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import UniversalPiwik from '../../src/piveau-universal-piwik.js';

const app = createApp(App);

app.use(UniversalPiwik, {
  router,
  isPiwikPro: import.meta.env.VUE_APP_TRACKER_IS_PIWIK_PRO && import.meta.env.VUE_APP_TRACKER_IS_PIWIK_PRO !== 'false',
  trackerUrl: import.meta.env.VUE_APP_TRACKER_TRACKER_URL || 'http://localhost:9090',
  siteId: import.meta.env.VUE_APP_TRACKER_SITE_ID || '1',
  pageViewOptions: {
    onlyTrackWithLocale: false,
    delay: 250,
    beforeTrackPageView: (from, to, tracker) => {
      console.log('beforeTrackPageView', from, to, tracker);
      tracker.trackDatasetDetailsPageView(null, null, {
        dataset_AccessRights: '',
        dataset_AccrualPeriodicity: '',
        dataset_Catalog: '',
        dataset_ID: '',
        dataset_Publisher: '',
        dataset_Title: '',
      });
    },
  },
  debug: import.meta.env.NODE_ENV === 'development',
  verbose: import.meta.env.NODE_ENV === 'development',
  immediate: false,
});

// app.config.productionTip = false

app.mount('#app');