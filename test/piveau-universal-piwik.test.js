import { describe, expect, it, vi } from 'vitest'
import UniversalPiwik from '../src/piveau-universal-piwik'
import { createApp } from 'vue-demi'
import { mount } from '@vue/test-utils'

vi.mock('../src/trackers/matomoTracker.js')

describe('UniversalPiwik Plugin', () => {

  it('registers $piwik object on Vue instance', () => {
    const app = createApp({ template: '<div></div>' })

    const wrapper = mount(app, {
      global: {
        plugins: [UniversalPiwik]
      }
    })

    expect(wrapper.vm.$piwik).toBeDefined()
    expect(wrapper.vm.$universalPiwik).toBeDefined()
  })
})
