import { beforeEach, afterEach, describe, expect, it, vi } from 'vitest'
import MatomoTracker from '../src/trackers/matomoTracker'

describe('MatomoTracker', () => {
  let mockDocument;

  beforeEach(() => {
    // Mock the window._paq array
    window._paq = [];

    // Create a mock script element
    const mockScriptElement = {
      type: '',
      async: false,
      defer: false,
      src: '',
    };

    // Mock the document methods
    mockDocument = {
      createElement: vi.fn().mockReturnValue(mockScriptElement),
      getElementsByTagName: vi.fn().mockReturnValue([{ parentNode: { insertBefore: vi.fn() } }]),
    };

    Object.defineProperty(global, 'document', { value: mockDocument, writable: true });
});

  afterEach(() => {
    vi.restoreAllMocks();
  });

  it('should initialize with default options', () => {
    const tracker = new MatomoTracker('https://mockurl.com', 'siteId123');

    expect(tracker.type).toEqual('matomo');
    expect(tracker.trackerUrl).toEqual('https://mockurl.com/');
    expect(tracker.siteId).toEqual('siteId123');

    // expect window.Matomo to be defined
    expect(window._paq).toBeDefined();
  });

  it('should init the tracker', () => {
    const tracker = new MatomoTracker('https://mockurl.com', 'siteId123', { debug: true });

    tracker.init();

    expect(window._paq).toContainEqual(['requireConsent']);
    expect(window._paq).toContainEqual(['setTrackerUrl', 'https://mockurl.com/matomo.php']);
    expect(window._paq).toContainEqual(['setSiteId', 'siteId123']);
    expect(mockDocument.createElement).toHaveBeenCalledWith('script');
  });

  it('should track page view with custom url and title', () => {
    const tracker = new MatomoTracker('https://mockurl.com', 'siteId123');

    tracker.trackPageView('/some/path', 'Some Title');

    expect(window._paq).toContainEqual(['setCustomUrl', '/some/path']);
    expect(window._paq).toContainEqual(['setDocumentTitle', 'Some Title']);
    expect(window._paq).toContainEqual(['trackPageView']);
  });

  it('should track event with category, action, name, and value', () => {
    const tracker = new MatomoTracker('https://mockurl.com', 'siteId123');

    tracker.trackEvent('SomeCategory', 'SomeAction', 'SomeName', 'SomeValue');

    expect(window._paq).toContainEqual(['trackEvent', 'SomeCategory', 'SomeAction', 'SomeName', 'SomeValue']);
  });

  it('should stop tracking and disable _paq', () => {
    const tracker = new MatomoTracker('https://mockurl.com', 'siteId123', { debug: true });

    tracker.stop();

    expect(tracker.isStopped).toBe(true);
    expect(typeof window._paq.push).toBe('function');
    expect(window._paq.push()).toBeUndefined(); // Since it's a mocked empty function now
  });
});
