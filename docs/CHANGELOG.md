# [3.3.0](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v3.2.0...v3.3.0) (2024-09-13)


### Features

* add documentTitleResolver as param ([08db479](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/08db479a090263bb8fb5354938b1ae2c48365c1f))

# [3.2.0](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v3.1.2...v3.2.0) (2024-08-16)


### Bug Fixes

* apply delay for matomo page tracking ([3d565f8](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/3d565f8707a782cf67a34b1c079be503d1d3e1e6))


### Features

* add requireConsent option to change between requireConsent and requireCookieConsent ([bb561c1](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/bb561c1783d03ac4404f34c1c48d378ced11c698))
* export trackRouteChanges ([cb453db](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/cb453db648ac15f22bd9b58da4e3a18b64ca2f86))

## [3.1.2](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v3.1.1...v3.1.2) (2024-07-29)


### Bug Fixes

* **README:** use accurate installation guide ([14beb73](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/14beb73d24c414ef9d12656e66296801eb4a43d5))

## [3.1.1](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v3.1.0...v3.1.1) (2023-09-12)


### Bug Fixes

* improve vue-router peerdeps ([db9a795](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/db9a795b6f49be83d5381ce6c55b6fd1258c7738))

# [3.1.0](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v3.0.0...v3.1.0) (2023-09-12)


### Features

* vue 2 compatibility ([11902ae](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/11902ae711d7e884c750a655cdfd1df4e482369e))

# [3.0.0](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.6.1...v3.0.0) (2023-09-11)


### Bug Fixes

* Upgrade to Vue 3 + Vite requires breaking change. ([1041326](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/1041326980ce0e4fc04fa3b1a4a897b0851d175b))


### BREAKING CHANGES

* Migrated from Vue 2 + Webpack to Vue 3 + Vite.
See Migration Guide v4 for more info.

## [2.6.1](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.6.0...v2.6.1) (2023-09-04)


### Bug Fixes

* router issues ([99bbf77](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/99bbf77df4b94a8604271724652199ff50721bf8))
* update dependencies and fix warnings and errors ([0c06a80](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/0c06a8023022ddbfe1a54206caec5b3ccd112eff))

# [2.6.0](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.5.1...v2.6.0) (2023-03-09)


### Features

* provide parameter for dimensions for trackDownload and trackOutlink ([9eb1b97](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/9eb1b9750244cd6512eab857954edf4deec4c724))

## [2.5.1](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.5.0...v2.5.1) (2022-09-05)


### Bug Fixes

* add suspend mode guards ([b8d9cfd](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/b8d9cfd7bb3c08f3068671069abc9464c1b1b256))

# [2.5.0](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.4.0...v2.5.0) (2022-08-24)


### Features

* implement piwik pro suspend--resume feataure ([ba34ac9](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/ba34ac99b6171f6003663cfd4f5858292ddaa782))
* implement trackPagePredicate option ([adb1005](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/adb10054dfbcdcb36be740debca84374ae9a9d6e))

# [2.4.0](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.3.0...v2.4.0) (2022-08-22)


### Features

* implement disabled option ([44e4126](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/44e41263f1da34d9f7ee7543860096d9a010937e))

# [2.3.0](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.2.0...v2.3.0) (2022-05-10)


### Features

* add beforeTrackPageView option in init opts ([a31156c](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/a31156cf40e488bdba10dbf8d42dab1ca531f400))

# [2.2.0](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.1.3...v2.2.0) (2022-05-10)


### Features

* add beforeTrackPageView hook ([7fcfeee](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/7fcfeeed7250079a308948659905bcd79a457184))

## [2.1.3](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.1.2...v2.1.3) (2022-04-04)


### Bug Fixes

* **package.json:** use correct module definition ([4d3932a](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/4d3932ad063b0006dd11d80ffc34cfd0b06641a9))

## [2.1.2](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.1.1...v2.1.2) (2021-06-23)


### Bug Fixes

* run initial trackPageView only after necessary other commands ([e7a7d20](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/e7a7d20b087fb4b245c608431a33ddb022fd3e53))

## [2.1.2-beta.1](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.1.1...v2.1.2-beta.1) (2021-06-23)


### Bug Fixes

* run initial trackPageView only after necessary other commands ([e7a7d20](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/e7a7d20b087fb4b245c608431a33ddb022fd3e53))

## [2.1.1](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.1.0...v2.1.1) (2021-06-23)


### Bug Fixes

* properly intercept link tracking command ([dc96c91](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/dc96c918b7b712400d84072b0283b0d6c6749655))

# [2.1.0](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.0.2...v2.1.0) (2021-06-22)


### Bug Fixes

* prevent sending screen_load event on initial navigation ([be558f5](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/be558f5df91745141b1bb51e15b3a390f292849b))


### Features

* implement removeCookiesWhenNoConsent, stopWhenNoConsent options ([5192277](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/51922772f35c71587fb5122bcaca3b1fdce34ae3))
* implement stop method ([77fed58](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/77fed58a7f68a8f16250d345782b3181ec5e7c98))

# [2.1.0-beta.2](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.1.0-beta.1...v2.1.0-beta.2) (2021-06-22)


### Features

* implement removeCookiesWhenNoConsent, stopWhenNoConsent options ([5192277](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/51922772f35c71587fb5122bcaca3b1fdce34ae3))

# [2.1.0-beta.1](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.0.3-beta.1...v2.1.0-beta.1) (2021-06-22)


### Features

* implement stop method ([77fed58](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/77fed58a7f68a8f16250d345782b3181ec5e7c98))

## [2.0.3-beta.1](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.0.2...v2.0.3-beta.1) (2021-06-22)


### Bug Fixes

* prevent sending screen_load event on initial navigation ([be558f5](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/be558f5df91745141b1bb51e15b3a390f292849b))

## [2.0.2](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.0.1...v2.0.2) (2021-06-15)


### Bug Fixes

* immediate option now work if set ([e22faf3](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/e22faf33088f30789b6701ad7d32dd8ada34665e))

## [2.0.2-beta.1](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.0.1...v2.0.2-beta.1) (2021-06-15)


### Bug Fixes

* immediate option now work if set ([e22faf3](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/e22faf33088f30789b6701ad7d32dd8ada34665e))

## [2.0.1](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.0.0...v2.0.1) (2021-06-09)

### Bug Fixes

* immediate option now work if set ([e22faf3](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/e22faf33088f30789b6701ad7d32dd8ada34665e))

## [2.0.1-beta.2](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.0.1-beta.1...v2.0.1-beta.2) (2021-06-15)


### Bug Fixes

* make piwikpro-matomo differentiation more robust ([e906cdb](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/e906cdb699e07e15814d747bddf2ca5efa61c502))

## [2.0.1-beta.1](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.0.0...v2.0.1-beta.1) (2021-06-09)


### Bug Fixes

* make piwikpro-matomo differentiation more robust ([e906cdb](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/e906cdb699e07e15814d747bddf2ca5efa61c502))

# [2.0.0](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v1.2.0...v2.0.0) (2021-06-07)


### Features

* add useDatasetsMinScoreFix option and disable it per default ([7b4cc8b](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/7b4cc8b4eed5ae809e31c09f776ef33688d8c8dd))


### BREAKING CHANGES

* disables page view ignore when on Datasets component unless there is a minScore query. to enable, set pageViewOptions.useDatasetsMinScoreFix to true
# [2.0.0-beta.2](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v2.0.0-beta.1...v2.0.0-beta.2) (2021-06-09)


### Bug Fixes

* make piwikpro-matomo differentiation more robust ([e906cdb](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/e906cdb699e07e15814d747bddf2ca5efa61c502))

# [2.0.0-beta.1](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v1.2.0-beta.2...v2.0.0-beta.1) (2021-06-07)


### Features

* add useDatasetsMinScoreFix option and disable it per default ([7b4cc8b](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/7b4cc8b4eed5ae809e31c09f776ef33688d8c8dd))


### BREAKING CHANGES

* disables page view ignore when on Datasets component unless there is a minScore query. to enable, set pageViewOptions.useDatasetsMinScoreFix to true
# [1.2.0](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v1.1.0...v1.2.0) (2021-06-07)


### Bug Fixes

* track page view at initial view when route is / ([b3de042](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/b3de04265e837052fc014e586612b97dcae086f6))


### Features

* add immediate option ([2ea845c](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/2ea845c3ebf548e381442eb2278e9cad2d361869))
* add verbose option ([4ed8f24](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/4ed8f2493a23d6eb0e91930e7f2abb035e749f55))

# [1.2.0-beta.2](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v1.2.0-beta.1...v1.2.0-beta.2) (2021-06-07)


### Bug Fixes

* track page view at initial view when route is / ([b3de042](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/b3de04265e837052fc014e586612b97dcae086f6))

# [1.2.0-beta.1](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v1.1.0...v1.2.0-beta.1) (2021-06-07)


### Features

* add immediate option ([2ea845c](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/2ea845c3ebf548e381442eb2278e9cad2d361869))
* add verbose option ([4ed8f24](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/4ed8f2493a23d6eb0e91930e7f2abb035e749f55))

# [1.1.0](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v1.0.0...v1.1.0) (2021-06-02)


### Features

* add trackEvent method ([7022446](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/70224466398d31964bea2101e65f16475bdcbe37))

# [1.1.0-beta.1](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v1.0.0...v1.1.0-beta.1) (2021-06-02)


### Features

* add trackEvent method ([7022446](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/70224466398d31964bea2101e65f16475bdcbe37))

# 1.0.0 (2021-06-02)


### Features

* initial release ([9e0423c](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/9e0423ce8cf00978296cd43b48a5f392d434bfd4))

# [1.0.0-beta.2](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/compare/v1.0.0-beta.1...v1.0.0-beta.2) (2021-06-02)


### Reverts

* Revert "fix(revertme): trigger release" ([5e70d8c](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/5e70d8c07a640384091b23f499d8ae93bc3d2b5a))

# 1.0.0-beta.1 (2021-06-02)


### Bug Fixes

* **revertme:** trigger release ([34d1eae](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/34d1eaeeae8e2bb7f08eb8df2e82fa173569aea7))


### Features

* initial release ([9e0423c](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/9e0423ce8cf00978296cd43b48a5f392d434bfd4))

# 1.0.0-true.1 (2021-06-02)


### Features

* initial release ([9e0423c](https://gitlab.fokus.fraunhofer.de/piveau/utilities/piveau-universal-piwik/commit/9e0423ce8cf00978296cd43b48a5f392d434bfd4))
